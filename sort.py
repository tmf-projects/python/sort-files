import glob
import os
import shutil

files_folder = ''

main_folder = 'Sort'

files_dir_names = ['text', 'archives', 'executable',
                   'java', 'photo', 'torrent',
                   'music', 'video', 'special']

files_extension = {
    files_dir_names[0]: [
        '.txt', '.py', '.doc',
        '.docx', '.pdf', '.xlsx',
        '.ppt', '.pptx', '.DOCX'
    ],
    files_dir_names[1]: [
        '.rar', '.zip'
    ],
    files_dir_names[2]: [
        '.exe', '.msi'
    ],
    files_dir_names[3]: [
        '.jar'
    ],
    files_dir_names[4]: [
        '.jpg', '.png'
    ],
    files_dir_names[5]: [
        '.torrent'
    ],
    files_dir_names[6]: [
        '.mp3', '.wav', '.ogg'
    ],
    files_dir_names[7]: [
        '.mp4'
    ],
    files_dir_names[8]: [
        '.pka', '.keystore'
    ]
}

files_list = []


def scanning():
    global files_list

    files_list = []
    if files_folder != '':
        os.chdir(files_folder)
    else:
        return

    for extension in files_extension:
        for ext in files_extension.get(extension):
            for file in glob.glob("*"+ext):
                if os.path.abspath(file) == os.path.abspath(__file__):
                    continue
                files_list.append(os.path.abspath(file))


def check_folders():
    if not os.path.isdir(main_folder):
        os.mkdir(main_folder)
        for directory in files_dir_names:
            if directory in os.listdir():
                shutil.move(directory, main_folder)
    os.chdir(main_folder)
    for directory in files_dir_names:
        if not os.path.isdir(directory):
            os.mkdir(directory)


def move():
    for directory in files_dir_names:
        for extension in files_extension:
            for ext in files_extension.get(extension):
                for file in files_list:
                    if os.path.splitext(os.path.basename(file))[1] == ext and ext in files_extension.get(directory):
                        shutil.move(file, directory)
                        files_list.remove(file)


def start():
    check_folders()
    move()
