from tkinter import *
from tkinter import messagebox as mbox
from tkinter import filedialog as fd
import sort


class App:
    def __init__(self, window: Tk):
        window.title("Sort")
        window.geometry("600x400+500+200")

        self.main_menu = Menu(window)
        window.config(menu=self.main_menu)
        self.main_menu.add_command(label="Новый путь", command=lambda: self.new_dir())
        self.main_menu.add_command(label="Начать сортировку", command=lambda: self.start())

        self.up_frame = Frame()
        self.main_frame = Frame()

        self.scroll = Scrollbar(self.main_frame)

        self.listbox = Listbox(self.main_frame, yscrollcommand=self.scroll.set)

        self.init()

    def init(self):
        self.init_frames()
        self.init_widgets()

    def init_frames(self):
        self.up_frame.pack(fill=X)
        self.main_frame.pack(fill=BOTH, expand=True)

    def init_widgets(self):
        self.scroll.pack(side=RIGHT, fill=Y)
        self.scroll.config(command=self.listbox.yview)

        self.listbox.pack(fill=BOTH, expand=True)

    def new_dir(self):
        directory = fd.askdirectory()
        sort.files_folder = directory
        sort.scanning()
        self.listbox.delete(0, END)
        for file in sort.files_list:
            self.listbox.insert(END, file)
        if len(sort.files_list) == 0:
            self.listbox.insert(END, "Программа не нашла файлы в директории "+"\""+sort.files_folder+"\"")

    def start(self):
        if len(sort.files_list) == 0:
            self.listbox.insert(END, "Вы не можете начать сортировку, программа не нашла файлы в директории "
                                + "\""+sort.files_folder+"\"")
            return
        sort.start()
        self.listbox.delete(0, END)
        self.listbox.insert(END, "Готово!")


root = Tk()
app = App(root)
root.mainloop()
